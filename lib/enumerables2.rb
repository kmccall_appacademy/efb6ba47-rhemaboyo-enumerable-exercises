require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(&:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? do |string|
    string.include?(substring)
  end
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  letters = string.split('')
  letters.select! do |letter|
    letters.count(letter) > 1 && letter != ' '
  end
  letters.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split
  sorted_words = words.sort_by(&:length)
  sorted_words[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ('a'..'z').to_a.reject do |letter|
    string.count(letter) > 0
  end
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).to_a.select do |year|
    not_repeat_year?(year)
  end
end

def not_repeat_year?(year)
  year.to_s == year.to_s.split('').uniq.join
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.each_with_index do |song, i|
    if song == songs[i + 1] || song == songs[i - 1]
      songs.delete(song)
    end
  end
  songs.uniq
end

def no_repeats?(song_name, songs)
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = string.split.map { |word| remove_punctuation(word) }
  words.reduce do |c_word, word|
    if c_distance(word) < c_distance(c_word)
      word
    else
      c_word
    end
  end
end

def c_distance(word)
  index = -1
  word.split('').reduce(word.length) do |distance, letter|
    index += 1
    length = word.length
    if letter == 'c'
      length - index
    else
      distance
    end
  end
end

def remove_punctuation(word)
  word.delete(', . / ; \' [ ] \ < > ? : \" { } | !')
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  idx = -1
  arr.reduce([]) do |range_array, num|
    idx += 1
    prev_el = arr[idx - 1]
    next_el = arr[idx + 1]
    if num == next_el && num != prev_el
      range_array << [idx]
    elsif num == prev_el && num != next_el
      range_array.last << idx
      range_array
    else
      range_array
    end
  end
end
